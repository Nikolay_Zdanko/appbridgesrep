//
//  PageViewController.swift
//  bigApp
//
//  Created by Nikolay on 9.04.21.
//

import UIKit

class PageViewController: UIPageViewController {
    
    lazy var myControllers: [UIViewController] = {
        return [
            newVC(name: "FirstStoryboard", viewController: "FirstViewController"),
            newVC(name: "SecondStoryboard", viewController: "SecondViewController"),
            newVC(name: "ThirdStoryboard", viewController: "ThirdViewController")
        ]
    }()
    var pageControl = UIPageControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        if let firstViewController = myControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        self.delegate = self
        setupPageControl()
    }
    
    func newVC(name: String, viewController: String) -> UIViewController {
        return UIStoryboard(name: name, bundle: nil).instantiateViewController(identifier: viewController)
    }
    
    func setupPageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = myControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = #colorLiteral(red: 0.5450980392, green: 0.537254902, blue: 1, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.8901111484, green: 0.8902574182, blue: 0.8900800347, alpha: 1)
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.5450980392, green: 0.537254902, blue: 1, alpha: 1)
        self.view.addSubview(pageControl)
    }

}

extension PageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myControllers.firstIndex(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else { return nil }
        guard myControllers.count > previousIndex else { return nil}
        return myControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myControllers.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard myControllers.count != nextIndex else { return nil}
        guard myControllers.count > nextIndex else { return nil }
        
        return myControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = myControllers.firstIndex(of: pageContentViewController)!
    }
}


