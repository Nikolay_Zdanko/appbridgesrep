//
//  UIView ->gradient.swift
//  bigApp
//
//  Created by Nikolay on 9.04.21.
//

import UIKit

extension UIView {
    func setGradient(view: UIView) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [
            UIColor(red: 0.367, green: 0.354, blue: 1, alpha: 1).cgColor,
            UIColor(red: 0.702, green: 0.696, blue: 1, alpha: 1).cgColor
        ]
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer.position = view.center
        
        self.layer.insertSublayer(layer, at: 0)
    }
}
