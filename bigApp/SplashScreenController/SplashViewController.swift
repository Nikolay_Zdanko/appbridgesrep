//
//  SplashViewController.swift
//  bigApp
//
//  Created by Nikolay on 8.04.21.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        delayPushController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.setGradient(view: self.view)
    }
    
    func delayPushController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            guard let controller = UIStoryboard(name: "LogInStoryboard", bundle: nil).instantiateViewController(identifier: "LogInViewController") as? LogInViewController else { return }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }


}

