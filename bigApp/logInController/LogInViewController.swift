//
//  LogInViewController.swift
//  bigApp
//
//  Created by Nikolay on 9.04.21.
//

import UIKit
import FirebaseAuth
import Firebase

class LogInViewController: UIViewController {
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0
    }
    
    @IBAction func toСomeInButton(_ sender: UIButton) {
        Auth.auth().signIn(withEmail: loginTextField.text ?? String(), password: passwordTextField.text ?? String()) { (result, error) in
            if error != nil {
                self.errorLabel.text = "error"
                self.errorLabel.alpha = 1
            } else {
                print("jump")
                guard let controller = UIStoryboard(name: "PageStoryboard", bundle: nil).instantiateViewController(identifier: "PageViewController") as? PageViewController else { return }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
        
    }
    
    
    @IBAction func registeredButtonAction(_ sender: UIButton) {
        guard let controller = UIStoryboard(name: "CheckInStoryboard", bundle: nil).instantiateViewController(identifier: "CheckInViewController") as? CheckInViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
