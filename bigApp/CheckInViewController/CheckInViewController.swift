//
//  CheckInViewController.swift
//  bigApp
//
//  Created by Nikolay on 9.04.21.
//

import UIKit
import FirebaseAuth
import Firebase

class CheckInViewController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var paswordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0
    }
    
    func checkError() -> String? {
        if nameTextField.text == "" ||
            mailTextField.text == "" ||
            paswordTextField.text == "" ||
            nameTextField.text == nil ||
            mailTextField.text == nil ||
            paswordTextField.text == nil {
            return "no errors"
        }
        return nil
    }
    
    @IBAction func registerButton(_ sender: UIButton) {
        if checkError() != nil {
            errorLabel.alpha = 1
            errorLabel.text = checkError()
        } else {
            Auth.auth().createUser(withEmail: mailTextField.text!, password: paswordTextField.text!) { (result, error) in
                if error != nil {
                    self.errorLabel.text = "\(error?.localizedDescription ?? String())"
                } else {
                    let db = Firestore.firestore()
                    db.collection("users").addDocument(data: [
                        "name": self.nameTextField.text!,
                        "uid": result!.user.uid
                    ]) { (error) in
                        if error != nil {
                            self.errorLabel.text = "Error label"
                        }
                        print(result!.user.uid)
                    }
                    print("jump to the next screen")
                }
            }
        } // else
    }
    
}
